FROM python:3.9

ENV PYTHONBUFFERED 1
ENV PYTHONWRITEBYTECODE 1

RUN useradd --user-group --create-home --no-log-init --shell /bin/bash twwapi

ENV APP_HOME=/home/twwapi/twwapi

WORKDIR $APP_HOME

COPY requirements.txt $APP_HOME
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY . $APP_HOME
RUN chown -R twwapi:twwapi $APP_HOME

USER twwapi:twwapi

ENTRYPOINT ["/home/twwapi/twwapi/entrypoint.sh"]

