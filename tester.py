import pysat.solvers as slv

import encoding2
import encoding
import heuristic
import networkx as nx


def get_tww(g: nx.Graph):
    best = None
    for c in nx.connected_components(g):
        if len(c) <= 1:
            continue
        sub = g.subgraph(c).copy()
        cb = get_tww_component(g.subgraph(c).copy())
        if best is None or cb[0] > best[0][0]:
            best = (cb, g)
    if best is None:
        return [0, []]
    return best[0]


def get_tww_component(g: nx.Graph):
    if not nx.is_connected(g):
        raise Exception("Error: Graph must be connected to use this code")
    enc = encoding.TwinWidthEncoding()
    #enc = encoding2.TwinWidthEncoding2(g)

    ub = heuristic.get_ub(g)
    ub2 = heuristic.get_ub2(g)
    ub = min(ub, ub2)
    cb = enc.run(g, slv.Cadical, ub, verbose=False)
    return cb
