from threading import Timer
from multiprocessing import Process, Manager
from flask import Flask, request
import networkx as nx

from tester import get_tww

TIMEOUT=5

def get_tww_return(graph, return_dict):
    cb = get_tww(graph)
    return_dict['ret'] = cb

api = Flask('twinwidth_api')
@api.route("/tww")
def api_access():
    g = nx.Graph()
    edgeStr = request.args['edgeList']
    try:
        for edge in edgeStr[2:-2].split("],["):
            a,b = edge.split(",")
            g.add_edge(a, b)
    except Exception as e:
        print(e)
        return {'Error': 'Malformed request'}, 400
    manager = Manager()
    return_dict = manager.dict()
    p = Process(target=get_tww_return, args=(g,return_dict))
    p.start()
    p.join(TIMEOUT)
    if p.is_alive():
        p.kill()
        return {'Error': 'Timeout'}, 503
    cb = return_dict['ret']
    contractions = [f"{cn},{cb[2][cn]}" for cn in cb[1][:-1]]
    return {'tww': cb[0],
            'contractions': contractions}
